import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})

export class ProfilePage {

  public get_domain = 'http://10.172.12.221:5000/user/test';
  public post_domain = 'http://10.172.12.221:5000/user/test';
  public calc_domain = 'http://10.172.2.246:5000/caffcalc/test';
  public usrname = 'Jeff';
  public weight = 60.5;
  public pregrant = false;
  public smoking = false;
  public miligrams = 30;
  public height = 1.94;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController,public http: Http) {

  }

  handleError(error) {
		console.log(error);
		return error.json().message || 'Server error, please try again later';
	}


  ngOnInit(){
    console.log("Ein Toast yuhuu");
    /*this.http.get(this.get_domain).subscribe(data => {
          console.log("Data Fetch");
          console.log(data);
          this.usrname = data.json().name;
          this.weight = data.json().weight;
          this.pregrant = data.json().pregnant;
          this.smoking = data.json().smoker;
          this.height = data.json().height;
      });//.catch(error => { console.log(error) });
      /*var headers = new Headers();

      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );

      let options = new RequestOptions({ headers: headers });
      let body = {
          dose: 20,
          time: 24
      };

      console.log("Sonnething?");
     this.http.post(this.calc_domain, body, options).subscribe(data => {console.log(data.json());});*/

  }

  updateDataToBank(){
    var headers = new Headers();

    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );

    let options = new RequestOptions({ headers: headers });
    let body = {
      weight:this.weight,
      pregnant:this.pregrant,
      smoker:this.smoking,
      height:this.height
    };
   this.http.put(this.post_domain, body).subscribe(data => {console.log(data);});
  }

  changeBodyMass() {
    let dialog = this.alertCtrl.create({
      title: 'Change body weight.',
      message: 'Please enter your new body weight into the field below or cancel.',
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update',
          handler: data => {
            console.log('Update clicked');
            console.log(data);

            this.weight=data.weight;

            this.updateDataToBank();
          }
        }
      ],
      inputs: [
        {
          name:"weight",
          placeholder:"Weight"
        }
      ]
    });
    dialog.present();
  }

  changeMilligrams() {
    let dialog = this.alertCtrl.create({
      title: 'Change coffeine limit.',
      message: 'Please set a new coffeine limit or cancel.',
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update',
          handler: data => {
            console.log('Update clicked');
            console.log(data);

            this.miligrams=data.mig;

            this.updateDataToBank();
          }
        }
      ],
      inputs: [
        {
          name:"mig",
          placeholder:"Milligrams"
        }
      ]
    });
    dialog.present();
  }


  // Set handler for yes or no
  changePregnacy() {
    let dialog = this.alertCtrl.create({
      title: 'Are you pregnant?',
      message: 'Choose whether or not you are pregnant or cancel.',
      buttons: [
        {
          text: 'No',
          handler: data => {
            console.log('NO clicked');
            this.pregrant=false;
            this.updateDataToBank();
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            console.log('Yes clicked');
            console.log(data);
            this.pregrant=true;
            this.updateDataToBank();
          }
        }
      ]
    });
    dialog.present();
  }

  // Set handler for yes or no
  changeSmoking() {
    let dialog = this.alertCtrl.create({
      title: 'Do you smoke?',
      message: 'Choose whether or not you smoke or cancel.',
      buttons: [
        {
          text: 'No',
          handler: data => {
            console.log('NO clicked');
            this.smoking=false;
            this.updateDataToBank();
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            console.log('Yes clicked');
            console.log(data);
            this.smoking=true;
            this.updateDataToBank();
          }
        }
      ]
    });
    dialog.present();
  }

  changeHeight() {
    let dialog = this.alertCtrl.create({
      title: 'Change height.',
      message: 'Please enter your new height into the field below or cancel.',
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update',
          handler: data => {
            console.log('Update clicked');
            console.log(data);

            this.height=data.height;

            this.updateDataToBank();
          }
        }
      ],
      inputs: [
        {
          name:"height",
          placeholder:"Height"
        }
      ]
    });
    dialog.present();
  }
}
