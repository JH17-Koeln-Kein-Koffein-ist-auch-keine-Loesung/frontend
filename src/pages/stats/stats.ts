

import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Platform } from 'ionic-angular';



@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html'
})
export class StatsPage {

   @ViewChild('barCanvas') barCanvas;
    @ViewChild('doughnutCanvas') doughnutCanvas;
    @ViewChild('lineCanvas') lineCanvas;

    barChart: any;
    doughnutChart: any;
    lineChart: any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public platform: Platform) {
  this.showAlert();
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Reminder',
      subTitle: 'You have to drink some more coffeine in about 6 hours to maintain your goal of 30mg.',
      buttons: ['OK']
    });
    alert.present();
  }



  shareTwitter() {
    let toast = this.alertCtrl.create({
    message: 'An image of the chart has been shared on your account'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();


    }

       ionViewDidLoad() {

        this.barChart = new Chart(this.barCanvas.nativeElement, {

            type: 'bar',
            data: {
                labels: ["Wed", "Thu" , "Fri", "Sat", "Sun"],
                datasets: [{
                    label: 'Amount in mg',
                    data: [50, 100, 175, 250, 90],
                    backgroundColor: [
                        'rgba(237, 201, 81, 0.5)',
                        'rgba(235, 104, 65, 0.5)',
                        'rgba(204, 42, 54, 0.5)',
                        'rgba(79, 55, 45, 0.5)',
                        'rgba(0, 160, 176, 0.5)'
                    ],
                    borderColor: [
                        'rgba(237, 201, 81, 1)',
                        'rgba(235, 104, 65, 1)',
                        'rgba(204, 42, 54, 1)',
                        'rgba(79, 55, 45, 1)',
                        'rgba(0, 160, 176, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }

        });

        this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

            type: 'doughnut',
            data: {
                labels: ["Tuesday", "Wednesday", "Thursday" , "Friday", "Saturday"],
                datasets: [{
                    label: 'Amount of Hours',
                    data: [7, 6, 8, 4, 2],
                    backgroundColor: [
                        'rgba(237, 201, 81, 0.5)',
                        'rgba(235, 104, 65, 0.5)',
                        'rgba(204, 42, 54, 0.5)',
                        'rgba(79, 55, 45, 0.5)',
                        'rgba(0, 160, 176, 0.5)'
                    ],
                    hoverBackgroundColor: [
                        'rgba(237, 201, 81, 1)',
                        'rgba(235, 104, 65, 1)',
                        'rgba(204, 42, 54, 1)',
                        'rgba(79, 55, 45, 1)',
                        'rgba(0, 160, 176, 1)'
                    ]
                }]
            }

        });

        this.lineChart = new Chart(this.lineCanvas.nativeElement, {

            type: 'line',
            data: {
                labels: ["0h", "3h", "6h", "9h", "12h", "15h", "18h", "21h"],
                datasets: [
                    {
                        label: "Dosis in mg",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(237, 201, 81, 0.5)",
                        borderColor: "rgba(237, 201, 81, 1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(237, 201, 81, 1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(237, 201, 81, 1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [90, 53.719, 32.063, 19.138, 11.423, 6.8179 , 4.0694 , 2.4289],
                        spanGaps: false,
                    }
                ]
            }

        });

    }


}
