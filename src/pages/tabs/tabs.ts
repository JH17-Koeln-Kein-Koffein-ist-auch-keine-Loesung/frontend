import { Component } from '@angular/core';

import { StatsPage } from '../stats/stats';
import { AchievementsPage } from '../achievements/achievements';
import { ProfilePage } from '../profile/profile';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = StatsPage;
  tab3Root = AchievementsPage;
  tab4Root = ProfilePage;

  constructor() {

  }
}
