import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {timestamp} from "rxjs/operator/timestamp";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  

  date: string = new Date().toISOString();

  id;
  dosis;
  timestamp = this.date;
  beverage_name;
  tokendosis;

  submit() {
    console.log(this.date);
    console.log(this.drink);
    console.log(this.dosis);
  }

  public selectdrink(): void {
    this.beverage_name = this.drink;
    console.log(this.beverage_name);
  }

  public getTime(): void {
    this.timestamp = this.date;
  }

  dosisForm() {
    this.tokendosis = this.dosis;
  }

  drink: string;

}
